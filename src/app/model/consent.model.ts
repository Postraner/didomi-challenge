interface AgreeType {
  name: string;
  selected: boolean;
}

export interface Consent {
    id: string;
    name: string;
    email: string;
    agreeType: AgreeType[];
}
