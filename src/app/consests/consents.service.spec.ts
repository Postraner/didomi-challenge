import { TestBed, inject } from '@angular/core/testing';

import { ConsentsService } from './consents.service';
import {BaseRequestOptions, Http} from '@angular/http';
import {MockBackend} from '@angular/http/testing';
import { consents } from '../backend/consents';

describe('ConsentsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        ConsentsService,
        MockBackend,
        BaseRequestOptions,
        {
          provide: Http,
          useFactory: (backend, options) => new Http(backend, options),
          deps: [MockBackend, BaseRequestOptions]
        }
      ],
    });

  });

  it('ConsentsService should be created', inject([ConsentsService],
    (consentsService: ConsentsService) => {
          expect(consentsService).toBeTruthy();
    }
  ));

  it('should return an Observable<Consent[]>', inject([ConsentsService],
    (consentsService: ConsentsService) => {
      consentsService.getConsents().subscribe((data) => {
        console.log(data);
        expect(data.length).toBe(1);
      });
    }
  ));

  it('should create new consent', inject([ConsentsService],
    (consentsService: ConsentsService) => {
      const newConsent = consents[0];

      consentsService.createConsent(newConsent).subscribe((data) => {
        expect(data).toBe(newConsent);
      });
    }
  ));

});
