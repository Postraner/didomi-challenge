import { ConsentsComponent } from './consests.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('Consents component', () => {
  let component: ConsentsComponent;
  let fixture: ComponentFixture<ConsentsComponent>;
  let consentsWrapper: DebugElement,
      contentWrapper: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ConsentsComponent],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsentsComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();

    consentsWrapper = fixture.debugElement.query(By.css('.consents-wrapper'));
    contentWrapper = fixture.debugElement.query(By.css('.content-wrapper'));
  });


  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('child elements should be created', () => {
    expect(consentsWrapper.nativeElement).toBeTruthy();
    expect(contentWrapper.nativeElement).toBeTruthy();
  });



});
