import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NewConsentsComponent } from './new-consents.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { ConsentsService } from '../consents.service';
import { Router, RouterModule } from '@angular/router';
import { appRoutes } from '../consents.routes';
import { CollectedConsentsComponent } from '../collected-consents/collected-consents.component';
import { MatTableModule } from '@angular/material';
import { APP_BASE_HREF, Location } from '@angular/common';
import { By } from '@angular/platform-browser';

describe('New consent component', () => {

  let location: Location;
  let router: Router;
  let component: NewConsentsComponent;
  let fixture: ComponentFixture<NewConsentsComponent>;
  let giveConsentBtn: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NewConsentsComponent,
        CollectedConsentsComponent
      ],
      imports: [
        ReactiveFormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),
        MatTableModule
      ],
      providers: [
        ConsentsService,
        {provide: APP_BASE_HREF, useValue : '/' },
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    router = TestBed.get(Router);
    location = TestBed.get(Location);
    fixture = TestBed.createComponent(NewConsentsComponent);
    router.initialNavigation();
    component = fixture.componentInstance;
    component.ngOnInit();

    giveConsentBtn = fixture.debugElement.query(By.css('.submit-btn')).nativeElement;
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.consentForm.valid).toBeFalsy();
  });

  it('email and name should be required', () => {
    const email = component.consentForm.controls['email'];
    const email_errors = email.errors || {};
    expect(email_errors['required']).toBeTruthy();

    const name = component.consentForm.controls['name'];
    const name_errors = name.errors || {};
    expect(name_errors['required']).toBeTruthy();
  });


  it('agree type should require at least one item', () => {
    const agreeType = component.consentForm.controls['agreeType'];
    let agreeTypeErrors = agreeType.errors || {};

    expect(agreeTypeErrors['multipleCheckboxRequireOne']).toBeTruthy();

    agreeType.patchValue([
      {
        name: 'some agree description',
        selected: true
      }
    ]);

    agreeTypeErrors = agreeType.errors || {};

    expect(agreeTypeErrors['multipleCheckboxRequireOne']).toBeFalsy();
  });

  it('fields should be valid if have valid value', () => {
    const email = component.consentForm.controls['email'];
    const name = component.consentForm.controls['name'];
    const agreeType = component.consentForm.controls['agreeType'];

    email.setValue('valid.email@gmail.com');
    name.setValue('Test name');
    agreeType.patchValue([
      {
        name: 'some agree description',
        selected: true
      }
    ]);

    expect(email.valid).toBeTruthy();
    expect(name.valid).toBeTruthy();
    expect(agreeType.valid).toBeTruthy();
  });

  it('submitting a form (form should be valid)', () => {
    expect(component.consentForm.valid).toBeFalsy();
    component.consentForm.controls['email'].setValue('test@test.com');
    component.consentForm.controls['name'].setValue('Test name');
    component.consentForm.controls['agreeType'].patchValue([
      {
        name: 'some agree description',
        selected: true
      }
    ]);
    expect(component.consentForm.valid).toBeTruthy();
    expect(component.submitted).toBeFalsy();

    component.onSubmit(component.consentForm.value);

    expect(component.submitted).toBeTruthy();
  });

  it('button should be disabled if agreeType is empty', () => {
    const agreeType = component.consentForm.controls['agreeType'];

    expect(agreeType.valid).toBeFalsy();

    agreeType.patchValue([
      {
        name: 'some agree description',
        selected: true
      }
    ]);

    expect(agreeType.valid).toBeTruthy();
    expect(giveConsentBtn.disabled).toBeFalsy();
  });

  it('after submit should redirect to collected consents', () => {
    component.consentForm.controls['email'].setValue('test@test.com');
    component.consentForm.controls['name'].setValue('Test name');
    component.consentForm.controls['agreeType'].patchValue([
      {
        name: 'some agree description',
        selected: true
      }
    ]);
    component.onSubmit(component.consentForm.value);

    expect(location.path()).toBe('/collected-consents');
  });



});
