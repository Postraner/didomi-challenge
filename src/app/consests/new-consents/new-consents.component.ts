import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  FormArray,
  Validators
} from '@angular/forms';

import { Router } from '@angular/router';
import { CustomValidators } from './custom.validators';
import { ConsentsService } from '../consents.service';

@Component({
  selector: 'app-new-consents',
  templateUrl: './new-consents.component.html',
  styleUrls: ['./new-consents.component.scss']
})
export class NewConsentsComponent implements OnInit {
  submitted = false;
  consent = {
    agreeType: [
      { name: 'Receive newsletter', value: 1, selected: false, id: 1 },
      { name: 'Be shown targeted ads', value: 2,  selected: false, id: 2 },
      { name: 'Contribute to anonymous visit statistics', value: 3, selected: false, id: 3 },
    ]
  };

  consentForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private consentsService: ConsentsService,
    private router: Router
  ) {
    this.consentForm = this.fb.group({
      name: ['', [
        Validators.required
      ]],
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      agreeType: this.buildAgreeType()
    });
  }

  ngOnInit() {
  }

  get agreeType(): FormArray {
    return this.consentForm.get('agreeType') as FormArray;
  }

  buildAgreeType() {
    const arr = this.consent.agreeType.map(a => {
      return this.fb.control(a.selected);
    });

    return this.fb.array(arr, CustomValidators.multipleCheckboxRequireOne);
  }

  onSubmit(form) {
    this.submitted = true;

    if (!this.consentForm.invalid) {
      const formValue = Object.assign({}, form, {
        agreeType: form.agreeType.map((selected, i) => {
            return {
              name: this.consent.agreeType[i].name,
              selected
            };
        })
      });

      this.consentsService.createConsent(formValue);
      this.router.navigate(['/collected-consents']);
    }
  }

}


