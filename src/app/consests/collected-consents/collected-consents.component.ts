import {Component, OnInit, ViewChild} from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { Consent } from '../../model/consent.model';
import { ConsentsService } from '../consents.service';
import { MatPaginator } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import _ from 'lodash';
import 'rxjs/add/operator/finally';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'app-collected-consents',
  templateUrl: './collected-consents.component.html',
  styleUrls: ['./collected-consents.component.scss']
})
export class CollectedConsentsComponent implements OnInit {
  consents: Consent[];
  isLoading = false;
  dataSource: ConsentDataSource | null;
  consentDatabase = new ConsentDatabase(this.consentsService);
  displayedColumns = ['name', 'email', 'agreeType'];

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource = new ConsentDataSource(
      this.consentDatabase,
      this.paginator
    );
  }

  constructor(private consentsService: ConsentsService) {}

  getAgreeTypeString(arr) {
    const selectedArray = _.filter(arr, (item) => item.selected);
    return selectedArray.reduce((result, item, index) => {
      return result += `${item.name}${((selectedArray.length - 1) !== index) ? ', ' : ''}`;
    }, '');
  }

}

export class ConsentDatabase {
  public dataChange: BehaviorSubject<Consent[]> = new BehaviorSubject<Consent[]>([]);

  get data(): Consent[] {
    return this.dataChange.value;
  }

  constructor(private consentsService: ConsentsService) {
    this.consentsService.getConsents().subscribe(data => this.dataChange.next(data));
  }
}

export class ConsentDataSource extends DataSource<any> {

  constructor(
    private _consentDatabase: ConsentDatabase,
    private _paginator: MatPaginator) {
   super();
  }

  connect(): Observable<Consent[]> {

    const displayDataChanges = [
      this._consentDatabase.dataChange,
      this._paginator.page
    ];

    return Observable.merge(...displayDataChanges)
      .startWith(null)
      .switchMap(() => {
        return Observable.of(this._consentDatabase.data);
      })
      .map((data) => {
        return data.slice(this._paginator.pageIndex, this._paginator.pageSize);
      })
      .catch((err) => {
        console.log(err);
        return Observable.of([]);
      });
  }

  disconnect() {}
}
