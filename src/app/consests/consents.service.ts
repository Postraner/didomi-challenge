import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Consent } from '../model/consent.model';

@Injectable()
export class ConsentsService {

    private static handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            if (error.status === 404) {
                errMsg = `Resource ${error.url} was not found`;
            } else {
                const body = error.json() || '';
                const err = body.error || JSON.stringify(body);
                errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
            }
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Observable.throw(errMsg);
    }

    constructor(private http: Http) {}

    getConsents(): Observable<Consent[]> {
        return this.http.get('/fake-backend/consents')
            .map(response => response.json() as Consent[])
            .catch(ConsentsService.handleError);
    }

    createConsent(consent: Consent): Observable<Consent> {
        return this.http.post('/fake-backend/consent', consent)
            .map(response => response.json() as Consent)
            .catch(ConsentsService.handleError);
    }
}
