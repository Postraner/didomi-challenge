import { NewConsentsComponent } from './new-consents/new-consents.component';
import { CollectedConsentsComponent } from './collected-consents/collected-consents.component';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
    { path: 'give-consent', component: NewConsentsComponent },
    { path: 'collected-consents', component: CollectedConsentsComponent },
    { path: '',   redirectTo: '/give-consent', pathMatch: 'full' }
];
