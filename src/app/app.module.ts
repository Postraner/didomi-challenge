import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { appRoutes } from './consests/consents.routes';

import { MockBackend } from '@angular/http/testing';
import { BaseRequestOptions } from '@angular/http';
import { HttpModule } from '@angular/http';
import { fakeBackendProvider } from './backend/mock-backend';

import { AppComponent } from './app.component';
import { ConsentsComponent } from './consests/consests.component';
import { NewConsentsComponent } from './consests/new-consents/new-consents.component';
import { CollectedConsentsComponent } from './consests/collected-consents/collected-consents.component';
import { ConsentsService } from './consests/consents.service';
import { LoaderComponent } from './shared/components/loader/loader.component';

import {
  MatFormFieldModule,
  MatButtonModule,
  MatCheckboxModule,
  MatInputModule,
  MatTabsModule,
  MatToolbarModule,
  MatTableModule,
  MatPaginatorModule
} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    ConsentsComponent,
    NewConsentsComponent,
    CollectedConsentsComponent,
    LoaderComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    HttpModule,

    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false }
    ),

    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatCheckboxModule,
    MatTabsModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule
  ],
  providers: [
    ConsentsService,
    fakeBackendProvider,
    MockBackend,
    BaseRequestOptions
  ],
  bootstrap: [AppComponent]

})
export class AppModule { }
