import {uuid} from './uuid';

export let consents = [
    {
        id: uuid.generate(),
        name: 'Max',
        email: 'max@gm.com',
        agreeType: [
          {
            name: 'Receive newsletter',
            selected: true
          },
          {
            name: 'Be shown targeted ads',
            selected: false
          },
          {
            name: 'Contribute to anonymous visit statistics',
            selected: false
          }
        ]
    },
];
