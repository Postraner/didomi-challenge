import {
    Http, 
    BaseRequestOptions, 
    Response, 
    ResponseOptions, 
    RequestMethod, 
    XHRBackend, 
    RequestOptions 
} from '@angular/http';

import { MockBackend, MockConnection } from '@angular/http/testing';

import { consents } from './consents';
import { uuid } from './uuid';
import { Consent } from '../model/consent.model';

function fakeBackendFactory(
    backend: MockBackend, 
    options: BaseRequestOptions, 
    realBackend: XHRBackend
) {

    let data: Consent[] = JSON.parse(localStorage.getItem('consents')) || consents;

    backend.connections.subscribe((connection: MockConnection) => {
        setTimeout(() => {
            if (connection.request.url.endsWith('/fake-backend/consents') &&
                connection.request.method === RequestMethod.Get) {
                connection.mockRespond(new Response(new ResponseOptions({
                    status: 200,
                    body: data
                })));

                return;
            }

            if (connection.request.url.endsWith('/fake-backend/consent') &&
                connection.request.method === RequestMethod.Post) {
                let receivedEmployee = JSON.parse(connection.request.getBody());
                let newEmployee = Object.assign(receivedEmployee, {id: uuid.generate()});
                data[data.length] = newEmployee;

                localStorage.setItem('consents', JSON.stringify(data));

                connection.mockRespond(new Response(new ResponseOptions({
                    status: 200,
                    body: newEmployee
                })));

                return;
            }

            let realHttp = new Http(realBackend, options);
            let requestOptions = new RequestOptions({
                method: connection.request.method,
                headers: connection.request.headers,
                body: connection.request.getBody(),
                url: connection.request.url,
                withCredentials: connection.request.withCredentials,
                responseType: connection.request.responseType
            });
            realHttp.request(connection.request.url, requestOptions)
                .subscribe((response: Response) => {
                        connection.mockRespond(response);
                    },
                    (error: any) => {
                        connection.mockError(error);
                    });
        }, 500);

    });

    return new Http(backend, options);
}

export let fakeBackendProvider = {
    provide: Http,
    useFactory: fakeBackendFactory,
    deps: [MockBackend, BaseRequestOptions, XHRBackend]
};